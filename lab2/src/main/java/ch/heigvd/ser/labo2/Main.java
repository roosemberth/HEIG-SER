/**
 * Noms des étudiants : Wilfried Karel Ngueukam, Roosembert Palacios
 */
package ch.heigvd.ser.labo2;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.function.BiConsumer;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import ch.heigvd.ser.labo2.coups.Case;
import ch.heigvd.ser.labo2.coups.ConvertissableEnPGN;
import ch.heigvd.ser.labo2.coups.CoupSpecial;
import ch.heigvd.ser.labo2.coups.Deplacement;
import ch.heigvd.ser.labo2.coups.Roque;
import ch.heigvd.ser.labo2.coups.TypePiece;
import ch.heigvd.ser.labo2.coups.TypeRoque;

class Main {
  public static void main(String... args) throws Exception {
    Document document = readDocument(new File("tournois_fse.xml"));

    Element root = document.getRootElement();
    List<Element> tournois = root.getChild("tournois").getChildren();

    writePGNfiles(tournois);
  }

  /**
   * Parse un fichier XML avec JDOM2.
   *
   * @param file Fichier XML à parser.
   */
  private static Document readDocument(File file)
      throws JDOMException, IOException {
    return (new SAXBuilder().build(file));
  }

  /**
   * Génère un fichier PGN pour chaque partie de chaque tournoi.
   * <p>
   * Le nom de chaque fichier PGN contient le nom du tournoi ainsi que le numéro
   * de la partie concernée.
   * <p>
   *
   * @param tournois Liste des tournois pour lesquelles écrire les fichiers PGN.
   */
  private static void writePGNfiles(List<Element> tournois) {
    for (Element tournoi : tournois) {
      forEachMatch(tournoi, (filename, partie) -> {
        writeMatch(new File(filename), partie);
      });
    }
  }

  /**
   * Écrit un fichier PGN pour le match passé en paramètre.
   *
   * @param file   Fichier où écrire le PGN
   * @param partie partie à sérialiser au format PGN
   */
  private static void writeMatch(File file, Element partie) {
    List<Element> coups = partie.getChild("coups").getChildren("coup");

    try (PrintWriter writer = new PrintWriter(
        new BufferedOutputStream(new FileOutputStream(file)))) {
      int tour = 0;
      for (int i = 0; i < coups.size(); ++i) {
        ++tour;
        Element coupBlanc = coups.get(i);

        String coupSpecial = coupBlanc.getAttributeValue("coup_special");
        if ("mat".equals(coupSpecial) || "nulle".equals(coupSpecial)) {
          writer.println(toMatchEntry(tour, coupBlanc, null));
          break;
        }

        if (++i == coups.size()) {
          // Partie non terminée.
          writer.println(toMatchEntry(tour, coupBlanc, null));
          break;
        }

        Element coupNoir = coups.get(i);
        writer.println(toMatchEntry(tour, coupBlanc, coupNoir));
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Retourne la ligne à écrire dans le fichier PGN.
   *
   * @param tour      numéro de tour dans la partie.
   * @param coupBlanc coup effectué par le joueur blanc.
   * @param coupNoir  coup effectué par le joueur noir.
   */
  private static String toMatchEntry(int tour, Element coupBlanc,
      Element coupNoir) {
    StringBuilder sb = new StringBuilder();
    sb.append(tour);
    sb.append(" ");
    sb.append(serializeCoup(coupBlanc).notationPGN());
    if (coupNoir != null) {
      sb.append(" ");
      sb.append(serializeCoup(coupNoir).notationPGN());
    }
    return sb.toString();
  }

  /**
   * Sérialize un coup de déplacement.
   *
   * @param déplacement Noeud déplacement à convertir.
   * @param coupSpecial S'il s'agit d'un coup spécial. Peut être null.
   */
  private static ConvertissableEnPGN serializeDéplacement(Element déplacement, CoupSpecial coupSpecial) {
    TypePiece deplacée = toPieceOrNull(déplacement.getAttributeValue("piece"));
    TypePiece eliminée = toPieceOrNull(déplacement.getAttributeValue("elimination"));
    TypePiece promue = toPieceOrNull(déplacement.getAttributeValue("promotion"));
    Case caseDépart = toCaseOrNull(déplacement.getAttributeValue("case_depart"));
    Case caseArrivée = toCaseOrNull(déplacement.getAttributeValue("case_arrivee"));

    try {
      return new Deplacement(deplacée, eliminée, promue, coupSpecial, caseDépart, caseArrivée);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Sérialize un coup au fotmat XML en {@link ConvertissableEnPGN}.
   *
   * @param coup Noeud XML avec le coup à convertir.
   */
  private static ConvertissableEnPGN serializeCoup(Element coup) {
    Element nodeDeplacement = coup.getChild("deplacement");
    CoupSpecial coupSpecial = toCoupSpecialOrNull(coup.getAttributeValue("coup_special"));

    if (nodeDeplacement != null)
      return serializeDéplacement(nodeDeplacement, coupSpecial);

    Element nodeRoque = coup.getChild("roque");
    if (nodeRoque != null)
      return new Roque(coupSpecial, toTypeRoqueOrNull(nodeRoque.getAttributeValue("type")));

    throw new RuntimeException("Le type de coup passé en paramètre est méconnu.");
  }

  /**
   * Retourne l'instance de la classe TypePiece correspondante pour le nom d'une pièce passé en paramètre, ou null si
   * ce n'est pas une pièce connue.
   * @param piece : Nom de la pièce dont on veut avoir le type de pièce
   * @return l'instance de la classe TypePiece correspondant à la pièce ou null
   */
  private static TypePiece toPieceOrNull(String piece) {
    if (piece == null)
      return null;
    switch (piece) {
    case "Cavalier":
      return TypePiece.Cavalier;
    case "Pion":
      return TypePiece.Pion;
    case "Dame":
      return TypePiece.Dame;
    case "Tour":
      return TypePiece.Tour;
    case "Roi":
      return TypePiece.Roi;
    case "Fou":
      return TypePiece.Fou;
    default:
      return null;
    }
  }

  /**
   * Retourne l'instance de la classe CoupSpecial correspondant pour un coup passé en paramètre ou null
   * @param coup : Nom du coup spécial
   * @return une instance de la classe CoupSpecial ou null
   */
  private static CoupSpecial toCoupSpecialOrNull(String coup) {
    if (coup == null)
      return null;
    switch (coup) {
    case "echec":
      return CoupSpecial.ECHEC;
    case "mat":
      return CoupSpecial.MAT;
    case "nulle":
      return CoupSpecial.NULLE;
    default:
      return null;
    }
  }

  /**
   * Retourne l'instance de la classe TypeRoque correspondant au nom du type roque passé en paramètre ou null
   * @param roque : Le nom du type de roque
   * @return l'instance de la classe TypeRoque correspondante ou null.
   */
  private static TypeRoque toTypeRoqueOrNull(String roque) {
    if (roque == null)
      return null;
    switch (roque) {
    case "petit_roque":
      return TypeRoque.PETIT;
    case "grand_roque":
      return TypeRoque.GRAND;
    default:
      return null;
    }
  }

  /**
   * Returne la case répresentée par la string en argument.
   *
   * @param c case sous forme String.
   */
  private static Case toCaseOrNull(String c) {
    if (c == null)
      return null;
    return new Case(c.charAt(0), Character.getNumericValue(c.charAt(1)));
  }

  /**
   * Appelle une fonction sur chaque partie du tournoi
   *
   * @param tournoi   Tournoi à sérialiser.
   * @param processor Fonction à appeller pour chaque match du tournoi.
   */
  private static void forEachMatch(Element tournoi, BiConsumer<String, Element> processor) {
    List<Element> parties = tournoi.getChild("parties").getChildren("partie");
    String tournoiName = tournoi.getAttributeValue("nom");
    for (int i = 0; i < parties.size(); i++) {
      String filename = tournoiName + " - Match " + i + ".txt";
      processor.accept(filename, parties.get(i));
    }
  }
}
