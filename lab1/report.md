---
toc: yes
date: Fri Mar 19 06:13:15 PM CET 2021
title: Enjoy Chess
subtitle: Modélisation XML et DTD
author:
- Roosembert Palacios
- Mai Anh Hoang
---

# Introduction

Dans ce rapport nous présentons la modélisation des tournois d'échecs proposés
par des associations.
Nous avons exprimé la modélisation proposé à l'aide d'un document de [définition
de type][dtd] XML avec un exemple.
Dans la suite du rapport nous discutons certains choix de modélisation et
des hypothèses de travail.

[dtd]: https://en.wikipedia.org/wiki/Document_type_definition

# Présentation du DTD

Nous avons basse la conception de notre schéma sur les propriétés suivantes :

- Empêcher au plus possible la représentation des états incohérents.
- Éviter le plus possible la répétition d'informations.
- Faire de sorte à ce que notre modèle soit facilement compréhensible sans
  connaître les détails de la représentation.

Nous avons néanmoins décidé de limiter la profondeur des vérifications,
notamment nous n'effectuons pas des vérification sur les propriétés suivantes:

- Que les mouvements du tableau sont valides
- Que deux pièces ne se chevauchent pas.
- Que le mouvement du roi ne réalise pas plusieurs actions au même temps.
- Que la fin du jeu est toujours dans les règles du jeu d'échecs.
- Que les captures d'autres pièces sont toujours possible.

En particulier, nous avons décidé de partir sur un modèle collections (e.g.
`players`, `referees`, `tournaments`, `games`) avec des références par moyen
d'un identifiant unique lorsqu'il s'agit des objets plus évolués.

Nous avons aussi décider de ne pas normaliser l'identité des arbitres étant
donné que l'énoncé ne nous spécifie pas suffisamment d'informations afin de
fournir une notion robuste d'identité.

Nous faisons aussi la remarque par rapport aux joueurs que le numéro de sécurité
sociale n'est pas un identifiant fiable (ou même tout le temps disponible).
Beaucoup de personnes pourraient décider de rejoindre un tournoi sans forcément
en posséder un (e.g. Mineurs, touristes, ...).
Nous avons donc décidé d'introduire une notion d'identifiant artificielle et
nous laissons l'application utilisant ces données définir leurs critères
d'acceptabilité par rapport aux numéros de sécurité sociale et comment gérer
ces cas limites.

Parmi les règles du jeu d'échecs, nous avons notamment modélisé :

- Que les tours de couleurs se suivent (e.g. 'noir' joue après 'blanc') :
  Chaque jeu possède une couleur de départ (par défaut 'blanc').
- Que chaque jeu a une fin.
- Que la fin de chaque jeux et soit un match remporté (`checkmate`), à égalité
  (`tie`), une capitulation (`surrender`) ou une annulation (`void`).
  Chaque fin donne lieu à des exprimer des remarques optionnelles.
- Position des pièces au début et fin du mouvement.
- Mouvement spéciales réalisés par les pièces, en particulier, nous avons prévus
  les cas où plusieurs actions se succèdent (e.g. Pion promu en Reine à l'aide
  d'un mouvement en passant qui a ensuite engendré un échec).

<!--

# Description de l'exemple fourni

Dans l'exemple en annexe nous illustrons un petit championnat avec quelques
parties de sorte à illustrer notre modélisation.
========= Inutile, si tu trouves quelque chose de meilleur, tu peux de commenter

-->

# Validation du DTD par un outil externe

![Screenshot providing 3rd party validation](images/screenshot-xml-dtd-valid.png)

# Questions

## Q1

> Imaginons que vous souhaitez enregistrer le classement ELO que chaque joueur
> d'une partie avait au moment où elle a été jouée. Qu'est-ce qu'il faudrait
> modifier dans votre DTD ?

Dans notre DTD, nous avons accès à l'ELO de chaque joueur à travers la partie
où il joue. Illustration:

> **match** -> teamX -> player1 -> **ELO**

Cependant, si nous voulons que ce soit plus direct, il faut rajouter deux
éléments dans l'élément *encounter* (affrontement): *player1* et *player2*. Ce
sont les deux joueurs qui participent à l'affrontement. Nous remplacerions donc
la ligne 30

> <!ELEMENT encounter EMPTY >
par
> <!ELEMENT encounter (player1, player2) >

## Q2

> Il est **possible** dans votre DTD de représenter le fait qu'il ne peut pas y
> avoir que 20 parties par tournoi. Comment faire ?

Comme nous n'avons pas d'expression qui permet de représenter le nombre exact
d'un élément dans un élément conteneur, nous devons mettre 20 fois l'élément
*match*. Dans notre DTD, nous avons mis un élément de liste de parties *matches*
 contenant plusieurs éléments *match*. La solution est donc de remplacer
 *(match)+* à la ligne 25, par 20 fois *match*. A noter que si nous voulons
 qu'il y a maximum 20 parties par tournoi, nous devons mettre 20 fois *match?*
 dans l'élément *matches*.

## Q3

> Est-ce possible dans votre DTD d'interdire le fait qu'une équipe joue
> contre elle-même dans une partie ?

Non, car nous pouvons assigner l'id d'une même équipe pour les deux attributs
*teamX* et *teamY* de l'élément *match*.

## Q4

> Est-ce possible dans votre DTD de vérifier que pour une partie l'arbitre fait
> bien partie du tournoi dans lequel la partie en question est jouée ?

Dans notre DTD, nous pouvons le faire. En effet, dans chaque affrontement
(*encounter*), il y a un attribut référençant un arbitre. Pour une partie
donnée, nous pouvons voir qui sont les arbitres. Comme l'élément *match*
(partie) fait partie de l'élément *tournament*, nous pouvons donc vérifier si un
arbitre fait partie d'un tournoi.
Illustration:

> **tournament** -> matches -> **match** -> encounter -> **referee**

# Conclusion
Au terme de ce laboratoire, nous avons appris à faire un DTD au travers de la 
mise en place de tournois d'échecs.

# Annexes

### DTD

```{.dtd fromfile=yes file=./association.dtd}
```

### XML d'exemple {.break}

```{.xml fromfile=yes file=./example.xml}
```
