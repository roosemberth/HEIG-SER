﻿<!ELEMENT association (associationName, players, referees, tournaments, games) >
<!ELEMENT associationName (#PCDATA) >

<!ELEMENT players (player)+ >
<!ELEMENT player (givenName, lastName, ssn, ELO) >
<!ATTLIST player playerId ID #REQUIRED >
<!ELEMENT givenName (#PCDATA) >
<!ELEMENT lastName (#PCDATA) >
<!ELEMENT ssn (#PCDATA) >
<!ELEMENT ELO (#PCDATA) >

<!ELEMENT referees (referee)+ >
<!ELEMENT referee (givenName, lastName) >
<!ATTLIST referee id ID #REQUIRED >

<!ELEMENT tournaments (tournament)+ >
<!ELEMENT tournament (teams, matches)+ >

<!ELEMENT teams (team)+ >
<!ELEMENT team (#PCDATA) >
<!ATTLIST team teamId  ID #REQUIRED >
<!ATTLIST team player1 IDREF #REQUIRED >
<!ATTLIST team player2 IDREF #REQUIRED >

<!ELEMENT matches (match)+ >
<!ELEMENT match (encounter, encounter, pointsX, pointsY)>
<!ATTLIST match teamX IDREF #REQUIRED>
<!ATTLIST match teamY IDREF #REQUIRED>
<!ATTLIST match setup (WX1BY1|WX1BY2) "WX1BY1">
<!ELEMENT encounter EMPTY >
<!ATTLIST encounter gameId  IDREF #REQUIRED>
<!ATTLIST encounter referee IDREF #REQUIRED>
<!ATTLIST encounter winner  (X|Y) #IMPLIED>
<!ELEMENT pointsX (#PCDATA)>
<!ELEMENT pointsY (#PCDATA)>

<!ELEMENT games (game)+ >
<!ELEMENT game (date, time, moves+)>
<!ATTLIST game id ID #REQUIRED >
<!ATTLIST game firstColor (W|B) "W" >
<!ELEMENT date (#PCDATA)>
<!ELEMENT time (#PCDATA)>
<!ELEMENT winner (W|B) >

<!ELEMENT moves ((bishop | king | knight | pawn | queen | rook )+, end) >
<!ELEMENT bishop (check?) >
<!ELEMENT king   (check?) >
<!ELEMENT knight (check?) >
<!ELEMENT pawn   (enpassant?, promotion?, check?) >
<!ELEMENT queen  (check?) >
<!ELEMENT rock   (check?) >
<!ATTLIST bishop from CDATA #REQUIRED to CDATA #REQUIRED >
<!ATTLIST knight from CDATA #REQUIRED to CDATA #REQUIRED >
<!ATTLIST pawn   from CDATA #REQUIRED to CDATA #REQUIRED >
<!ATTLIST queen  from CDATA #REQUIRED to CDATA #REQUIRED >
<!ATTLIST rook   from CDATA #REQUIRED to CDATA #REQUIRED >
<!ATTLIST bishop capture (bishop | knight | pawn | queen | rook) #IMPLIED >
<!ATTLIST king   capture (bishop | knight | pawn | queen | rook) #IMPLIED >
<!ATTLIST knight capture (bishop | knight | pawn | queen | rook) #IMPLIED >
<!ATTLIST pawn   capture (bishop | knight | pawn | queen | rook) #IMPLIED >
<!ATTLIST queen  capture (bishop | knight | pawn | queen | rook) #IMPLIED >
<!ATTLIST rook   capture (bishop | knight | pawn | queen | rook) #IMPLIED >
<!-- The king may perform well-defined movements -->
<!ATTLIST king   from CDATA #IMPLIED to CDATA #IMPLIED >
<!ATTLIST king   special (kingside | queenside | surrender) #IMPLIED >

<!ELEMENT enpassant EMPTY >
<!ELEMENT promotion EMPTY >
<!ATTLIST promotion toPiece (bishop | knight | pawn | queen | rook) #REQUIRED >
<!-- List of pieces checking the king -->
<!ELEMENT check (eBishop | eKing | eKnight | ePawn | eQueen | eRook)+ >
<!ATTLIST check kingPos CDATA #REQUIRED kingColor (W|B) #REQUIRED >
<!ELEMENT eBishop EMPTY >
<!ELEMENT eKing   EMPTY >
<!ELEMENT eKnight EMPTY >
<!ELEMENT ePawn   EMPTY >
<!ELEMENT eQueen  EMPTY >
<!ELEMENT eRock   EMPTY >
<!ATTLIST eBishop position CDATA #REQUIRED >
<!ATTLIST eKing   position CDATA #REQUIRED >
<!ATTLIST eKnight position CDATA #REQUIRED >
<!ATTLIST ePawn   position CDATA #REQUIRED >
<!ATTLIST eQueen  position CDATA #REQUIRED >
<!ATTLIST eRook   position CDATA #REQUIRED >

<!-- Final notes -->
<!ELEMENT end (#PCDATA) >
<!ATTLIST end type (checkmate | void | tie | surrender) #REQUIRED >
