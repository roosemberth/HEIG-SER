# SER lab3

## Running the App

```console
mvn exec:java
```

## Running the tests

```console
mvn test
```

## Package & run from jar

```console
mvn clean package
java -jar target/*-jar-with-dependencies.jar
```
