---
date: 4 May 2021
title: "Lab3: JSON => KML"
author:
  - Roosembert Palacios
  - Kylian Bourcoud 
---

## Descriptif des classes

```plantuml
@startuml
skinparam monochrome true
hide methods

class GeoJsonParser {
  +GeoJsonParser(filename : String);
  +CountryFile getList();
  -Polygon parsePolygon(coordinates : JSONArray)
}

class KMLFileWriter {
  void KMLFileWriter(countryFile : CountryFile);
}

class CountryFile

GeoJsonParser -r-> CountryFile : Create
KMLFileWriter -l-> CountryFile : Use

class Country {
  name : string
  iso_a3 : string
}

class Polygon {
  
}

class Coordinates {
  latitude : string
  latitude : string
}

CountryFile "1" --> "*" Country : Contain
Country "1" *--> "1..*" Polygon : Contain
Polygon "1" *--> "1..*" Coordinates : Contain

@enduml
```

### Classes de traitement de données

#### GeoJsonParser

Cette classe va lire un document geojson et le parser pour obtenir un objet
CountryList qui représente une liste de pays.

#### KMLWriter

Cette classe va convertir un objet CountryList en un document KML.

### Classes du domaine

#### CountryList

Cette classe représente un modèle de liste de pays.

#### Country

Cette classe représente un modèle de pays.

#### Polygon

Cette classe représente les différents territoires d'un pays

#### Coordinate

Cette classe représente une coordonnées (longitude, latitude) d'un point d'un
polygone

---

## Difficultés rencontrés

- Comprendre le format geojson, surtout la partie sur les polygones.

- Trouver le bon type de container à utiliser pour répresenter les pays et
  ses territoires dans le KML car _Google earth_ affiche une iconne différente
  en fonction du type de container et on voulait s'approcher au plus possible
  de la capture d'écran.

---

## Problèmes connus et non corrigés

Aucun bug connu, par contre l'architecture des classes aurait pu être revu
(ex: remplacer la classe `CountryList` par une `List<Country>`).

À cause d'une limitation fixée par la donnée, tous les pays doivent être chargé
en mémoire (et en plusieurs copies sous formats différentes). Ceci n'aurait pas
été le cas si on aurait utilisé une lib qui permet de génerer le XML au fur et
à mesure (i.e. _streamming_).

---

## Parser JSON {.break}

![Parser Screenshot](./img/parser.JPG){framewidth=6cm}

---

## KML

![Screenshot de l'application _Google Earth_ après avoir importé le KML géneré
](./img/google-earth-screenshot-after-loading-countries.png)
---

## Nos apprentissages

- Apprendre à lire un fichier json avec la librairie jsonSimple
- Comprendre le format geojson
- Comprendre le format KML et son utilisation par google earth

---

## Conclusion

Ce labo nous a permis de nous familiariser avec la lecture d'un fichier json et
réviser l'écriture d'un fichier xml.
De plus, ils nous a permis de connaître certains format plus précis de fichiers:
geojson pour json et kml pour xml.

Nous certifions par nos signature que ce travail est sans plagiat.

![Signature Kylian](./img/signatureKylian.png){framewidth=1.5cm}

![Signature Roosembert](./img/signatureRoos.png){framewidth=1.5cm}

