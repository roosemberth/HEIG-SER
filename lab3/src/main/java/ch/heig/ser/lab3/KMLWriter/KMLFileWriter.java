package ch.heig.ser.lab3.KMLWriter;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import ch.heig.ser.lab3.Model.Coordinate;
import ch.heig.ser.lab3.Model.Country;
import ch.heig.ser.lab3.Model.CountryList;
import ch.heig.ser.lab3.Model.Polygon;

public class KMLFileWriter {
    CountryList countryList;

    public KMLFileWriter(CountryList countryList) {
        this.countryList = countryList;
    }

    private Element toKml() {
        Element root = new Element("Document");

        Element docName = new Element("name");
        docName.addContent("Countries");
        root.addContent(docName);

        for (Country country : countryList.getCountries())
            root.addContent(countryElement(country));

        return root;
    }

    public void writeTo(String path) {
        XMLOutputter xo = new XMLOutputter(Format.getPrettyFormat());
        try (PrintWriter fileWriter = new PrintWriter(
                new BufferedOutputStream(new FileOutputStream(path)))) {
            xo.output(toKml(), fileWriter);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Element countryElement(Country country) {
        Element countryElement = new Element("Placemark");

        Element name = new Element("name");
        name.addContent(country.getName());
        countryElement.addContent(name);

        Element extrude = new Element("extrude");
        extrude.addContent("1");
        countryElement.addContent(extrude);

        Element tessellate = new Element("tessellate");
        tessellate.addContent("1");
        countryElement.addContent(tessellate);

        Element altitudeMode = new Element("altitudeMode");
        altitudeMode.addContent("relativeToGround");
        countryElement.addContent(altitudeMode);

        countryElement.addContent(getStyle());

        Element territories = new Element("MultiGeometry");
        for (Polygon poly : country.getBorder()) {
            Element territory = new Element("Polygon");
            Element oB = new Element("outerBoundaryIs");
            Element lr = new Element("LinearRing");
            Element coords = new Element("coordinates");
            for (Coordinate coord : poly.getCoordinates()) {
                String latitude = coord.getLatitude();
                String longitude = coord.getLongitude();
                coords.addContent(longitude + "," + latitude + ",0\n");
            }
            lr.addContent(coords);
            oB.addContent(lr);
            territory.addContent(oB);
            territories.addContent(territory);
        }
        countryElement.addContent(territories);

        return countryElement;
    }

    private Element getStyle() {
      Element outline = new Element("outline");
      outline.addContent("1");
      Element fill = new Element("fill");
      fill.addContent("0");

      Element polyStyle = new Element("PolyStyle");
      polyStyle.addContent(outline);
      polyStyle.addContent(fill);

      Element style = new Element("Style");
      style.addContent(polyStyle);
      return style;
    }
}
