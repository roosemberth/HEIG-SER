package ch.heig.ser.lab3.Model;

/**
 * Classe représentant des coordonnées
 */
public class Coordinate {

    private String longitude;
    private String latitude;

    /**
     * Constructeur de la classe
     * @param longitude la longitude initiale
     * @param latitude la latitude initiale
     */
    public Coordinate(String longitude, String latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     * Getter de la longitude
     * @return la longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Setter de la longitude
     * @param longitude la nouvelle longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * getter de la latitude
     * @return la latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * setter de la latitude
     * @param latitude la nouvelle latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
