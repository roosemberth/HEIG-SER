package ch.heig.ser.lab3.Model;

import java.util.LinkedList;

/**
 * Classe représentant un territoire d'un pays
 */
public class Polygon {
    LinkedList<Coordinate> coordinates;

    /**
     * Constructeur du territoire
     * @param coordinates la liste des coordonnées qui forme le territoire
     */
    public Polygon(LinkedList<Coordinate> coordinates){
        this.coordinates = coordinates;
    }

    /**
     * Getter de la liste des coordonnées du territoire
     * @return la liste des coordonnées
     */
    public LinkedList<Coordinate> getCoordinates() {
        return coordinates;
    }
}
