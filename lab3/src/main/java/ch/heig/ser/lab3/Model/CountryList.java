package ch.heig.ser.lab3.Model;

import java.util.LinkedList;

/**
 * Classe représentant une liste de pays
 */
public class CountryList {

    LinkedList<Country> countries;

    /**
     * Constructeur
     * @param countries une liste de pays
     */
    public CountryList(LinkedList<Country> countries) {
        this.countries = countries;
    }

    /**
     * Getter de la liste des pays
     * @return la liste des pays
     */
    public LinkedList<Country> getCountries() {
        return countries;
    }

    /**
     * Setter de la liste des pays
     * @param countries la liste des pays
     */
    public void setBorder(LinkedList<Country> countries) {
        this.countries = countries;
    }
}
