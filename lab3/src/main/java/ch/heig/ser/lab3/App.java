package ch.heig.ser.lab3;

import ch.heig.ser.lab3.KMLWriter.KMLFileWriter;
import ch.heig.ser.lab3.Model.CountryList;
import ch.heig.ser.lab3.ParserJson.GeoJsonParser;

public class App {
    public static void main(String[] args) {
        if (args.length > 0
                && ("-h".equals(args[0]) || "--help".equals(args[0])))
            showUsageAndTerminate(0);

        if (args.length != 2)
            showUsageAndTerminate(1);

        CountryList countryList = new GeoJsonParser(args[0]).getList();
        KMLFileWriter writer = new KMLFileWriter(countryList);
        writer.writeTo(args[1]);
    }

    private static void showUsageAndTerminate(int statusCode) {
        // @formatter:off
        System.out.println(
            "Converts a geojson file to a KML.\n" +
            "\n" +
            "Usage: java -jar ser-lab3-0.1.jar <geojson> <output filename>\n" +
            "\n" +
            "<geojson> must be a geojson file (see http://geojson.org).\n" +
            "<output filename> can be any file, but it is recommended to use " +
            "the KML extension.\n" +
            "\n" +
            "Example: \n" +
            "  java -jar ser-lab3-0.1.jar countries.geojson countries.kml"
        );
        System.exit(statusCode);
        // @formatter:on
    }
}
