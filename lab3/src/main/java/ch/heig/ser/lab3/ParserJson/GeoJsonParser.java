package ch.heig.ser.lab3.ParserJson;

import ch.heig.ser.lab3.Model.Coordinate;
import ch.heig.ser.lab3.Model.Country;
import ch.heig.ser.lab3.Model.CountryList;
import ch.heig.ser.lab3.Model.Polygon;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * Cette classe va parser un document geojson précis et renvoyer un objet CountryList avec la méthode getList()
 */
public class GeoJsonParser {

    /**
     * Liste des pays générés
     */
    private final LinkedList<Country> countries;

    /**
     * Le constructeur va parser le fichier
     * et stocker la liste des pays obtenus dans l'attribut countries
     * @param filename le nom du fichier a parser
     */
    public GeoJsonParser(String filename){

        // initialisation de la liste des pays
        countries = new LinkedList<>();

        // Obtention du parser json
        JSONParser jsonParser =new JSONParser();

        // Récupération du fichier en lecture
        try (FileReader reader = new FileReader(filename)) {

            // Récupération du tableau des pays
            Object obj = jsonParser.parse(reader);
            JSONArray tmpCountries = (JSONArray) ((JSONObject) obj).get("features");

            // Récupération des données pour chaque pays
            for (Object count : tmpCountries){

                JSONObject country = (JSONObject) count;
                JSONObject properties = (JSONObject) country.get("properties");

                // Récupération des données du pays
                String countryName = (String) properties.get("ADMIN");
                String iso_a3 = (String) properties.get("ISO_A3");
                iso_a3 = iso_a3.equals("-99") ? null : iso_a3;

                JSONObject geometry = (JSONObject) country.get("geometry");

                // Récupération du type de polygon (Polygon | MultiPolygon)
                String type = (String) geometry.get("type");

                // Récupération des polygons
                JSONArray Polygons = (JSONArray) geometry.get("coordinates");

                // Sortie console pour la capture d'écran
                System.out.println("(" + iso_a3 + ") " + countryName);

                LinkedList<Polygon> polygons = null;

                // Récupération des polygones selon le type
                if (type.equals("Polygon")){
                    polygons = new LinkedList<>();
                    polygons.add(parsePolygon(Polygons));
                } else if (type.equals("MultiPolygon")) {
                    polygons = new LinkedList<>();
                    for (Object polygon : Polygons) {
                        JSONArray polygons2 = (JSONArray) polygon;
                        polygons.add(parsePolygon(polygons2));
                    }
                }

                // ajout du pays à la liste
                countries.add( new Country(countryName, iso_a3, polygons) );
            }
        }  catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Renvoie la liste des pays générés
     * @return la liste des pays
     */
    public CountryList getList(){
        return new CountryList(countries);
    }

    /**
     * Méthodes privés qui va parser un tableau json Polygon
     * @param coordinatesArray le tableau des coordonnées
     * @return un objet Polygon
     */
    private Polygon parsePolygon(JSONArray coordinatesArray){
        LinkedList<Coordinate> coordinateList = new LinkedList<>();


        JSONArray coordinateArray2 = (JSONArray) coordinatesArray.get(0);

        // Sortie console pour la capture d'écran
        System.out.println("\t- " + coordinateArray2.size() + " coordinates");

        //Récupération de chaque coordonnées du polygon
        for (Object coordinateObject : coordinateArray2) {
            JSONArray coordinate = (JSONArray) coordinateObject;
            double longitude = (double) coordinate.get(0);
            double latitude = (double) coordinate.get(1);

            // ajout de la coordonnées à la liste de coordonnées
            coordinateList.add(new Coordinate( Double.toString(longitude), Double.toString(latitude)) );

        }
        // construction et retour d'un objet polygon
        return new Polygon(coordinateList);
    }
}
