package ch.heig.ser.lab3.Model;

import java.util.LinkedList;

/**
 * Classe représentant les données d'un pays
 */
public class Country {

    private String name;
    private String iso_a3;
    private LinkedList<Polygon> border;

    /**
     * Constructeur
     * @param name le nom du pays
     * @param iso_a3 la notation iso A3 associé au pays
     * @param border La liste de tous les polygones formant les frontières
     */
    public Country(String name, String iso_a3, LinkedList<Polygon> border) {
        this.name = name;
        this.iso_a3 = iso_a3;
        this.border = border;
    }

    /**
     * getter du nom du pays
     * @return le nom
     */
    public String getName() {
        return name;
    }

    /**
     * Setter du nom du pays
     * @param name le nom du pays
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter de la norme iso a3
     * @return iso a3
     */
    public String getIso_a3() {
        return iso_a3;
    }

    /**
     * setter de la norme iso a3
     * @param iso_a3 la norme iso a3
     */
    public void setIso_a3(String iso_a3) {
        this.iso_a3 = iso_a3;
    }

    /**
     * Getter de la liste des territoires du pays
     * @return la liste des territoires
     */
    public LinkedList<Polygon> getBorder() {
        return border;
    }

    /**
     * Setter de la liste des territoires du pays
     * @param border la nouvelle liste des territoires
     */
    public void setBorder(LinkedList<Polygon> border) {
        this.border = border;
    }
}
